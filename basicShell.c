/*
Basic Shell
Nathan Gibson
*/
#include <stdio.h>

int main()
{
	int count = 0;
	char line[100];
	char *tokens[100];
	
	printf("Basic Shell\n");
	printf("> Initializing: \n");

	While (1)
	{
		//Print a prompt
		printf("> ");
		//Get input from stdin
		fgets(input, 100, stdin);
		
		//Tokenize input
		char *token
		token = strtok(line, " \n\t");
		tokens[count] = token;
		count++;
		
		while (token != NULL)
		{
			token = strtok(NULL, " \n\t");
			tokens[count] = token;
			count++;
		}
		
		//Fork
		pid_t pid = fork();
		if (pid == 0)
		{//parent
			pid = wait(&status);
			//Check for return value from wait
			//Return value contains PID of child
			//Status contains exit code in second byte
				//child status = status & 0xff
				//exit code = status >> 8
			exit(0);
		}
		else if (pid > 0)
		{//child
			int retval = execvp(tokens[0], tokens);
			if (retval == -1)
			{//error
				fprintf(stderr, "Exec Failed: %s\n", stderr(errno);
				exit(1);
			}
			exit(0);
		}
		else
		{//error
			fprintf(stderr, "Fork Failed: %s\n", stderr(errno));
			exit(1);
		}
	}
	//Go back to printing a prompt
}
